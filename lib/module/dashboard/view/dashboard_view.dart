import 'package:_9codeid/module/dashboard/widget/Menu.dart';
import 'package:flutter/material.dart';
import 'package:_9codeid/core.dart';
import '../controller/dashboard_controller.dart';

class DashboardView extends StatefulWidget {
  const DashboardView({Key? key}) : super(key: key);

  Widget build(context, DashboardController controller) {
    controller.view = this;
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        title: Text("9code.id"),
        actions: [],
        backgroundColor: Color(0xFFa435f0),
        titleTextStyle: TextStyle(
          color: Colors.white,
          fontSize: 16.0,
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            children: const [
              Menu(),
            ],
          ),
        ),
      ),
    );
  }

  @override
  State<DashboardView> createState() => DashboardController();
}
