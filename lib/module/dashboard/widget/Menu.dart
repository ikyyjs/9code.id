// ignore_for_file: camel_case_types, prefer_typing_uninitialized_variables

import 'package:_9codeid/module/reuseable_widget/view/reuseable_widget_view.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Menu extends StatelessWidget {
  const Menu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Map<String, dynamic>> menuItems = [
      {
        "label": "Reuseable Widget",
        "page": ReuseableWidgetView(),
        "icon": Icons.favorite
      },
    ];
    return GridView.builder(
      padding: EdgeInsets.zero,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        childAspectRatio: 1 / 0.3,
        crossAxisCount: 2,
        mainAxisSpacing: 6,
        crossAxisSpacing: 6,
      ),
      itemCount: menuItems.length,
      shrinkWrap: true,
      physics: ScrollPhysics(),
      itemBuilder: (BuildContext context, int index) {
        var item = menuItems[index];
        return InkWell(
          onTap: () {
            Get.to(item["page"]);
          },
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(8.0),
            ),
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Row(
                children: [
                  Expanded(
                    child: Text(
                      item["label"],
                      style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Icon(
                    item["icon"],
                    size: 20.0,
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
