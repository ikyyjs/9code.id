import 'package:_9codeid/shared/widget/category_picker/qcategory_picker.dart';
import 'package:_9codeid/shared/widget/date_picker/qdate_picker.dart';
import 'package:_9codeid/shared/widget/image_picker/qimage_picker.dart';
import 'package:_9codeid/shared/widget/time_picker/qtime_picker.dart';
import 'package:dio/dio.dart';
import 'package:_9codeid/shared/widget/banner_image/banner.dart';
import 'package:_9codeid/shared/widget/textfield/qtextfield.dart';
import 'package:_9codeid/shared/widget/listview/qlistview.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import '../controller/reuseable_widget_controller.dart';
import 'package:_9codeid/core.dart';
import 'package:get/get.dart';

class UserService {
  static getUsers(page) async {}
}

class ReuseableWidgetView extends StatelessWidget {
  const ReuseableWidgetView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ReuseableWidgetController>(
      init: ReuseableWidgetController(),
      builder: (controller) {
        controller.view = this;

        return Scaffold(
          body: SingleChildScrollView(
            controller: ScrollController(),
            child: Container(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Materi Banner Image",
                    style: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    "- Widget secara terpisah\n"
                    "- Widget ditaruh di /lib/shared/widget/namawidget\n"
                    "- Widget dapat digunakan di seluruh module\n"
                    "- Widget sudah menerapkan props / dinamis",
                    style: TextStyle(
                      fontSize: 12.0,
                    ),
                  ),
                  const SizedBox(
                    height: 10.0,
                  ),
                  BannerImage(
                    url: "https://picsum.photos/300",
                    title: "Flash Sale",
                    subTitle: "Discount 90%",
                    info: "Today!",
                  ),
                  const SizedBox(
                    height: 10.0,
                  ),
                  BannerImage(
                    url: "https://picsum.photos/1000",
                    title: "Sementara Kopi",
                    subTitle: "Discount 55%",
                    info: "Terbaekkk!",
                  ),
                  const SizedBox(
                    height: 20.0,
                  ),
                  Text(
                    "Materi Text Field",
                    style: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    "- Widget secara terpisah\n"
                    "- Widget ditaruh di /lib/shared/widget/namawidget\n"
                    "- Widget dapat digunakan di seluruh module\n"
                    "- Widget sudah menerapkan props / dinamis\n"
                    "- Widget bisa menangkap apa yang diketik",
                    style: TextStyle(
                      fontSize: 12.0,
                    ),
                  ),
                  const SizedBox(
                    height: 10.0,
                  ),
                  QTextField(
                    label: "Email",
                    ikon: Icons.email,
                    tangkap: (value) {
                      print("Email: $value");
                    },
                  ),
                  const SizedBox(
                    height: 10.0,
                  ),
                  QTextField(
                    label: "Password",
                    ikon: Icons.password_rounded,
                    tangkap: (value) {
                      print("Password: $value");
                    },
                    obscureText: true,
                  ),
                  const SizedBox(
                    height: 10.0,
                  ),
                  QTextField(
                    label: "Address",
                    ikon: Icons.map,
                    tangkap: (value) {
                      print("Address: $value");
                    },
                  ),
                  const SizedBox(
                    height: 20.0,
                  ),
                  Text(
                    "Materi List View + HTTP Request",
                    style: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    "- Widget secara terpisah\n"
                    "- Widget ditaruh di /lib/shared/widget/namawidget\n"
                    "- Widget dapat digunakan di seluruh module\n"
                    "- Widget sudah menerapkan props / dinamis",
                    style: TextStyle(
                      fontSize: 12.0,
                    ),
                  ),
                  const SizedBox(
                    height: 10.0,
                  ),
                  QListView(
                    enableSearch: true, // feature listview search jadi true
                    future: (page, tangkap) async {
                      print("page: $page");
                      print("page: $tangkap");

                      var url =
                          "http://192.168.1.4:3000/users?page=$page&search=$tangkap";
                      print(url);

                      var response = await Dio().get(
                        url,
                        options: Options(
                          headers: {
                            "Content-Type": "application/json",
                          },
                        ),
                      );
                      Map obj = response.data;
                      return obj["data"];
                    },
                    itemBuilder: (Map item, int index) {
                      return Card(
                        child: ListTile(
                          leading: CircleAvatar(
                            backgroundColor: Colors.grey[200],
                            backgroundImage: NetworkImage(
                              item["avatar"],
                            ),
                          ),
                          title: Text("${item["first_name"]}"),
                          subtitle: Text("id.${item["id"]} ${item["email"]}"),
                        ),
                      );
                    },
                  ),
                  const SizedBox(
                    height: 20.0,
                  ),
                  Text(
                    "Materi Category Picker",
                    style: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    "- Widget secara terpisah\n"
                    "- Widget ditaruh di /lib/shared/widget/namawidget\n"
                    "- Widget dapat digunakan di seluruh module\n"
                    "- Widget sudah menerapkan props / dinamis",
                    style: TextStyle(
                      fontSize: 12.0,
                    ),
                  ),
                  const SizedBox(
                    height: 10.0,
                  ),
                  QCategoryPicker(
                    // *
                    items: [
                      // *
                      {"label": "Male", "value": "Male"},
                      {"label": "Female", "value": "Female"},
                    ],
                    onItemSelected: (index, label, value, item) {
                      // *
                      print(index);
                      print(label);
                      print(value);
                      print(item);
                    },
                  ),
                  const SizedBox(
                    height: 20.0,
                  ),
                  Text(
                    "Materi Date Picker",
                    style: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    "- Widget secara terpisah\n"
                    "- Widget ditaruh di /lib/shared/widget/namawidget\n"
                    "- Widget dapat digunakan di seluruh module\n"
                    "- Widget sudah menerapkan props / dinamis",
                    style: TextStyle(
                      fontSize: 12.0,
                    ),
                  ),
                  const SizedBox(
                    height: 10.0,
                  ),
                  QDatePicker(
                    label: "Date",
                    value: DateTime.now(),
                    onChanged: (value, formattedValue) {
                      print("value : $value");
                      print("formattedValue : $formattedValue");
                    },
                  ),
                  const SizedBox(
                    height: 20.0,
                  ),
                  Text(
                    "Materi Time Picker",
                    style: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    "- Widget secara terpisah\n"
                    "- Widget ditaruh di /lib/shared/widget/namawidget\n"
                    "- Widget dapat digunakan di seluruh module\n"
                    "- Widget sudah menerapkan props / dinamis",
                    style: TextStyle(
                      fontSize: 12.0,
                    ),
                  ),
                  const SizedBox(
                    height: 10.0,
                  ),
                  QTimePicker(
                    label: "Working hours",
                    value: TimeOfDay(hour: 17, minute: 00),
                    onChanged: (value, formattedValue) {
                      print("value : $value");
                      print("formattedValue : $formattedValue");
                    },
                  ),
                  const SizedBox(
                    height: 10.0,
                  ),
                  QTimePicker(
                    label: "End hours",
                    value: TimeOfDay(hour: 09, minute: 00),
                    onChanged: (value, formattedValue) {
                      print("value : $value");
                      print("formattedValue : $formattedValue");
                    },
                  ),
                  const SizedBox(
                    height: 20.0,
                  ),
                  Text(
                    "Materi Image Picker",
                    style: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    "- Widget secara terpisah\n"
                    "- Widget ditaruh di /lib/shared/widget/namawidget\n"
                    "- Widget dapat digunakan di seluruh module\n"
                    "- Widget sudah menerapkan props / dinamis",
                    style: TextStyle(
                      fontSize: 12.0,
                    ),
                  ),
                  const SizedBox(
                    height: 10.0,
                  ),
                  QImagePicker(
                    label: "Photo",
                    onChanged: (value) {},
                  ),
                  const SizedBox(
                    height: 20.0,
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
