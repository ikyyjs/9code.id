import 'package:get/get.dart';
import '../view/reuseable_widget_view.dart';

class ReuseableWidgetController extends GetxController {
  ReuseableWidgetView? view;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
