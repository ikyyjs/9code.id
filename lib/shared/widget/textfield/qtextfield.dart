// ignore_for_file: camel_case_types, prefer_typing_uninitialized_variables

import 'package:flutter/material.dart';

class QTextField extends StatefulWidget {
  final String label;
  final IconData ikon;
  final Function(String) tangkap;
  final bool? obscureText;

  const QTextField({
    Key? key,
    required this.label,
    required this.ikon,
    required this.tangkap,
    this.obscureText = false,
  }) : super(key: key);

  @override
  State<QTextField> createState() => _QTextFieldState();
}

class _QTextFieldState extends State<QTextField> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60.0,
      padding: const EdgeInsets.symmetric(
        horizontal: 16.0,
      ),
      decoration: BoxDecoration(
        color: Colors.grey[200],
        borderRadius: const BorderRadius.all(
          Radius.circular(
            6.0,
          ),
        ),
        border: Border.all(
          width: 1.0,
          color: Colors.grey[300]!,
        ),
      ),
      child: Center(
        child: TextField(
          obscureText: widget.obscureText!,
          style: TextStyle(
            color: Colors.grey[800],
          ),
          decoration: InputDecoration(
            hintText: widget.label,
            suffixIcon: Icon(
              widget.ikon,
              color: Colors.grey[600],
            ),
            border: InputBorder.none,
            hintStyle: TextStyle(
              color: Colors.grey[600],
            ),
          ),
          onSubmitted: (value) {
            widget.tangkap(value);
          },
        ),
      ),
    );
  }
}
