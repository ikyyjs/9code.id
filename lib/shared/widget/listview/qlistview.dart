import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class QListView extends StatefulWidget {
  final Function(int page, String tangkap) future; // FEATURE SEARCH
  final Function(Map item, int index) itemBuilder;
  final bool enableSearch;

  const QListView({
    Key? key,
    required this.future,
    required this.itemBuilder,
    this.enableSearch = false, // FEATURE SEARCH
  }) : super(key: key);

  @override
  State<QListView> createState() => _QListViewState();
}

class _QListViewState extends State<QListView> {
  List items = [];
  int page = 1;
  String tangkap = ''; // FEATURE SEARCH
  ScrollController scrollController = ScrollController();

  @override
  void initState() {
    loadData();
    scrollController.addListener(() {
      var offset = scrollController.offset;
      var maxOffset = scrollController.position.maxScrollExtent;

      if (offset == maxOffset) {
        page++;
        loadData();
        print("mentok nih");
      }
    });
    super.initState();
  }

  loadData() async {
    var newItems = await widget.future(page, tangkap); // FEATURE SEARCH
    items.addAll(newItems);
    setState(() {});
  }

  reloadData() async {
    page = 1;
    items.clear();
    await loadData();
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () => reloadData(),
      child: Column(
        children: [
          SizedBox(
            height: 42,
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.white,
              ),
              onPressed: () => reloadData(),
              child: const Row(
                children: [
                  Expanded(
                    child: Text("Refresh"),
                  ),
                  Icon(
                    Icons.refresh,
                    size: 24.0,
                  ),
                ],
              ),
            ),
          ),
          // FEATURE SEARCH
          const SizedBox(
            height: 12.0,
          ),
          if (widget.enableSearch)
            Column(
              children: [
                Container(
                  height: 50.0,
                  padding: const EdgeInsets.symmetric(
                    horizontal: 16.0,
                  ),
                  decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: const BorderRadius.all(
                      Radius.circular(
                        6.0,
                      ),
                    ),
                  ),
                  child: Center(
                    child: TextField(
                      style: TextStyle(
                        color: Colors.grey[800],
                      ),
                      decoration: InputDecoration(
                        hintText: "Search",
                        prefixIcon: Icon(
                          Icons.search,
                          color: Colors.grey[600],
                        ),
                        border: InputBorder.none,
                        hintStyle: TextStyle(
                          color: Colors.grey[600],
                        ),
                      ),
                      onSubmitted: (value) {
                        tangkap = value;
                        page = 1;
                        items.clear();
                        loadData();
                      },
                    ),
                  ),
                ),
              ],
            ),
          // FEATURE SEARCH
          const SizedBox(
            height: 12.0,
          ),
          Column(
            children: [
              SizedBox(
                height: 500,
                child: ListView.builder(
                  controller: scrollController,
                  itemCount: items.length,
                  shrinkWrap: true,
                  physics: ScrollPhysics(),
                  itemBuilder: (BuildContext context, int index) {
                    var item = items[index];
                    return widget.itemBuilder(item, index);
                  },
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
