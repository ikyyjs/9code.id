// ignore_for_file: camel_case_types, prefer_typing_uninitialized_variables

import 'dart:io';

import 'package:dio/dio.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class QImagePicker extends StatefulWidget {
  final String label;
  final String? hint;
  final String? value;
  final Function(String value) onChanged;
  const QImagePicker({
    Key? key,
    required this.label,
    this.hint,
    this.value,
    required this.onChanged,
  }) : super(key: key);

  @override
  State<QImagePicker> createState() => _QImagePickerState();
}

class _QImagePickerState extends State<QImagePicker> {
  late TextEditingController textEditingController;
  String? value;

  @override
  void initState() {
    value = widget.value;
    textEditingController = TextEditingController(
      text: value ?? "-",
    );
    super.initState();
  }

  uploadFile(String? filePath) async {
    if (filePath == null) return;
    textEditingController.text = "Uploading...";
    await Future.delayed(Duration(seconds: 2));

    final formData = FormData.fromMap({
      'image': MultipartFile.fromBytes(
        File(filePath).readAsBytesSync(),
        filename: "upload.jpg",
      ),
    });

    var res = await Dio().post(
      'http://192.168.1.4:3000/image',
      data: formData,
    );

    var data = res.data["data"] as List<dynamic>;
    var url = data[0]["url"].toString();

    value = url;

    setState(() {});
    textEditingController.text = "Upload complete!";
    await Future.delayed(Duration(seconds: 1));
    textEditingController.text = value!;
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () async {
        //
        if (Platform.isAndroid || Platform.isIOS) {
          XFile? image = await ImagePicker().pickImage(
            source: ImageSource.gallery,
            imageQuality: 40,
          );
          String? filePath = image?.path;
          uploadFile(filePath);
        } else {
          FilePickerResult? result = await FilePicker.platform.pickFiles(
            type: FileType.custom,
            allowedExtensions: [
              "png",
              "jpg",
            ],
            allowMultiple: false,
          );
          if (result == null) return;
          File file = File(result.files.single.path!);
          String? filePath = file.path;
          uploadFile(filePath);
        }
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TextFormField(
            controller: textEditingController,
            enabled: false,
            decoration: InputDecoration(
              labelText: widget.label,
              labelStyle: TextStyle(
                color: Colors.blueGrey,
              ),
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(
                  color: Colors.blueGrey,
                ),
              ),
              suffixIcon: Icon(Icons.image),
            ),
            onChanged: (value) {},
          ),
          if (value != null) ...[
            const SizedBox(
              height: 10.0,
            ),
            Image.network(
              value!,
              width: 200.0,
              height: 200.0,
              fit: BoxFit.cover,
            ),
          ]
        ],
      ),
    );
  }
}
