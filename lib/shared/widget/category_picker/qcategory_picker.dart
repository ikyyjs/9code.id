// ignore_for_file: camel_case_types, prefer_typing_uninitialized_variables

import 'package:flutter/material.dart';

class QCategoryPicker extends StatefulWidget {
  final List<Map<String, dynamic>> items; // *
  final Function(
    int index,
    String label,
    dynamic value,
    Map<String, dynamic> item,
  ) onItemSelected; // *

  const QCategoryPicker({
    Key? key,
    required this.items, // *
    required this.onItemSelected, // *
  }) : super(key: key);

  @override
  State<QCategoryPicker> createState() => _QCategoryPickerState();
}

class _QCategoryPickerState extends State<QCategoryPicker> {
  // *
  int selectedIndex = -1;
  updateIndex(newIndex) {
    selectedIndex = newIndex;
    setState(() {});
    var item = widget.items[selectedIndex];
    var index = selectedIndex;
    var label = item["label"];
    var value = item["value"];
    widget.onItemSelected(index, label, value, item);
  }
  // *

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SingleChildScrollView(
          controller: ScrollController(),
          scrollDirection: Axis.horizontal,
          child: Row(
            children: List.generate(widget.items.length, (index) {
              // *
              bool selected = selectedIndex == index; // *

              var item = widget.items[index]; // *

              return InkWell(
                onTap: () => updateIndex(index), // *

                child: Card(
                  color: selected ? Colors.indigo : null, // *
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      item["label"], // *
                      style: TextStyle(
                        fontSize: 12.0,
                        color: selected ? Colors.white : null, // *
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              );
            }),
          ),
        ),
      ],
    );
  }
}
