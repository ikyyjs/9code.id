// ignore_for_file: camel_case_types, prefer_typing_uninitialized_variables

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class QDatePicker extends StatefulWidget {
  final String label;
  final String? hint;
  final DateTime? value;
  final Function(DateTime date, String formattedDate) onChanged;

  const QDatePicker({
    Key? key,
    required this.label,
    this.hint,
    this.value,
    required this.onChanged,
  }) : super(key: key);

  @override
  State<QDatePicker> createState() => _QDatePickerState();
}

class _QDatePickerState extends State<QDatePicker> {
  DateTime? value;
  late TextEditingController textEditingController;

  @override
  void initState() {
    // TODO: implement initState
    textEditingController = TextEditingController(
      text: formattedValue,
    );
    value = widget.value;
    super.initState();
  }

  String? get formattedValue {
    if (value != null) {
      return DateFormat("d MMM y").format(value!);
    }
  }

  Widget build(BuildContext context) {
    return InkWell(
      onTap: () async {
        DateTime? pickedDate = await showDatePicker(
          context: context,
          initialDate: DateTime.now(),
          firstDate: DateTime(2000),
          lastDate: DateTime(2100),
        );
        print("pickedDate: $pickedDate");
        if (pickedDate != null) {
          value = pickedDate;
          setState(() {});
          textEditingController.text = formattedValue!;
          widget.onChanged(value!, formattedValue!);
        }
      },
      child: TextFormField(
        controller: textEditingController,
        enabled: false,
        decoration: InputDecoration(
          labelText: widget.label,
          labelStyle: TextStyle(
            color: Colors.blueGrey,
          ),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: Colors.blueGrey,
            ),
          ),
          suffixIcon: Icon(Icons.date_range),
          helperText: widget.hint,
        ),
        onChanged: (value) {},
      ),
    );
  }
}
