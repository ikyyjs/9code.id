// ignore_for_file: camel_case_types, prefer_typing_uninitialized_variables

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class QTimePicker extends StatefulWidget {
  final String label;
  final String? hint;
  final TimeOfDay? value;
  final Function(TimeOfDay date, String formattedTime) onChanged;

  const QTimePicker({
    Key? key,
    required this.label,
    this.hint,
    this.value,
    required this.onChanged,
  }) : super(key: key);

  @override
  State<QTimePicker> createState() => _QTimePickerState();
}

class _QTimePickerState extends State<QTimePicker> {
  TimeOfDay? value;
  late TextEditingController textEditingController;

  @override
  void initState() {
    // TODO: implement initState
    textEditingController = TextEditingController(
      text: formattedValue,
    );
    value = widget.value;
    super.initState();
  }

  String? get formattedValue {
    if (value != null) {
      return "${value!.hour.toString().padLeft(2, '0')}:${value!.minute.toString().padLeft(2, '0')}";
    }
    return null;
  }

  Widget build(BuildContext context) {
    return InkWell(
      onTap: () async {
        TimeOfDay? pickedTime = await showTimePicker(
          initialTime: TimeOfDay.now(),
          context: context,
          builder: (context, child) {
            return MediaQuery(
              data:
                  MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true),
              child: child ?? Container(),
            );
          },
        );
        print("pickedTime: $pickedTime");
        print("pickedDate: $pickedTime");
        if (pickedTime != null) {
          value = pickedTime;
          setState(() {});
          textEditingController.text = formattedValue!;
          widget.onChanged(value!, formattedValue!);
        }
      },
      child: TextFormField(
        controller: textEditingController,
        enabled: false,
        decoration: InputDecoration(
          labelText: widget.label,
          labelStyle: TextStyle(
            color: Colors.blueGrey,
          ),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: Colors.blueGrey,
            ),
          ),
          suffixIcon: Icon(Icons.timer),
          helperText: widget.hint,
        ),
        onChanged: (value) {},
      ),
    );
  }
}
