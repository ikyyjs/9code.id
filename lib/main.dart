//#TEMPLATE main_getx
import 'package:_9codeid/core.dart' as myCore;
import 'package:_9codeid/module/dashboard/view/dashboard_view.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:window_manager/window_manager.dart';

// flutter pub add get
// flutter pub add window_manager

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // // // SETUP WINDOWS
  await windowManager.ensureInitialized();
  await windowManager.setSize(Size(360.0, 640.0));
  windowManager.setAlwaysOnTop(true);

  runApp(
    GetMaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData.dark(),
      home: DashboardView(),

      // NAVIGATOR BACK DI WINDOWS
      builder: (context, child) {
        return Material(
          child: Stack(
            children: [
              child!,
              Positioned(
                right: 10,
                bottom: 20,
                child: InkWell(
                  onTap: () => Get.back(),
                  child: CircleAvatar(
                    radius: 12.0,
                    backgroundColor: Colors.white,
                    child: Icon(
                      Icons.chevron_left,
                      color: Color(0xFFa435f0),
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    ),
  );
}
//#END
